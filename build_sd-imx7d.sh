#!/bin/bash

#
# Initial settings
#

NAME=$(basename $0)
U_BOOT_IMG="u-boot.imx"
KERNEL_IMG="zImage"
DTB_IMG="board.dtb"
ROOTFS_IMG="rootfs.tgz"
MOD_FW_H_IMG="linux-mod-fw-h.tgz"
SWAP_SIZE="128M"
EXTRA_DEBS_DIR="./extra_debs"
EXTRA_FILES_DIR="./extra_files"

#
# Functions
#

function usage() {
	echo -e "Build a complete SD image form imx7d based boards" >&2
	echo -e "note: you must be root to execute this program!" >&2
	echo -e "" >&2
	echo "usage: $NAME [OPTIONS] <device>" >&2
	echo -e "" >&2
	echo -e "\twhere OPTIONS are:" >&2
	echo -e "\t  -h|--help              : display this help and exit" >&2
	echo -e "\t  --u-boot[=<img>]       : use the u-boot image <img> (default $U_BOOT_IMG)" >&2
	echo -e "\t  --kernel[=<img>]       : use the kernel image <img> (default $KERNEL_IMG)" >&2
	echo -e "\t  --dtb[=<img>]          : use the DTB image <img> (default $DTB_IMG)" >&2
	echo -e "\t  --rootfs[=<img>]       : use the rootfs image <img> (default $ROOTFS_IMG)" >&2
	echo -e "\t  --mod-fw-h[=<img>]     : use the modules, firmware & headers image <img> (default $MOD_FW_H_IMG)" >&2
	echo -e "\t  --add-swap[=<size>]    : add also a swap file <size> large (default $SWAP_SIZE)" >&2
	echo -e "\t  --extra-debs[=<dir>]   : install extra packages from <dir> in destination / (default $EXTRA_DEBS_DIR)" >&2
	echo -e "\t  --extra-files[=<dir>]  : copy extra files from <dir> in destination / (default $EXTRA_FILES_DIR)" >&2
        exit 1
}

#
# Main
#

# Check command line
TEMP=$(getopt -o h \
	--long help,u-boot::,kernel::,dtb::,mod-fw-h::,rootfs::,add-swap::,extra-debs::,extra-files:: \
	-n $NAME -- "$@")
[ $? != 0 ] && exit 1
eval set -- "$TEMP"
while true ; do
        case "$1" in
	-h|--help)
                usage
                ;;

        --u-boot)
		u_boot_img=$U_BOOT_IMG
                [ -n "$2" ] && u_boot_img=$2
                shift 2
        ;;

        --kernel)
		kernel_img=$KERNEL_IMG
                [ -n "$2" ] && kernel_img=$2
                shift 2
        ;;

        --dtb)
		dtb_img=$DTB_IMG
                [ -n "$2" ] && dtb_img=$2
                shift 2
        ;;

        --rootfs)
		rootfs_img=$ROOTFS_IMG
                [ -n "$2" ] && rootfs_img=$2
                shift 2
        ;;

        --mod-fw-h)
		mod_fw_h_img=$MOD_FW_H_IMG
                [ -n "$2" ] && mod_fw_h_img=$2
                shift 2
        ;;

        --add-swap)
		swap_size=$SWAP_SIZE
		[ -n "$2" ] && swap_size=$2
                shift 2
        ;;

        --extra-debs)
		extra_debs_dir=$EXTRA_DEBS_DIR
                [ -n "$2" ] && extra_debs_dir=$2
                shift 2
        ;;

        --extra-files)
		extra_files_dir=$EXTRA_FILES_DIR
                [ -n "$2" ] && extra_files_dir=$2
                shift 2
        ;;

        --)
                shift
                break
                ;;

        *)
                echo "$NAME: internal error!" >&2
                exit 1
                ;;
        esac
done

# Check command line
if [ $# -lt 1 ] ; then
        usage
fi
dev=$1
if [ ! -b $dev ] ; then
	echo "$NAME: block device $dev doesn't exist or not a block device!" >&2
	exit 1
fi

# Build partitions names
if echo $dev | grep -q "mmcblk" ; then
	devp1=${dev}p1
	devp2=${dev}p2
else
	devp1=${dev}1
	devp2=${dev}2
fi

# Check for root user
if [ "$(whoami)" != "root" ] ; then
        echo "$NAME: you must be root to execute this program!" >&2
        exit 1
fi

# Display current content
echo -e "-------------------------------------------------------------------------------"
[ -n "$u_boot_img" ] && echo "$NAME: u-boot image is $u_boot_img"
[ -n "$kernel_img" ] && echo "$NAME: kernel image is $kernel_img"
[ -n "$dtb_img" ] && echo "$NAME: DTB image is $dtb_img"
[ -n "$rootfs_img" ] && echo "$NAME: rootfs image is $rootfs_img"
[ -n "$mod_fw_h_img" ] && echo "$NAME: modules, firmware & headers image is $mod_fw_h_img"
[ -n "$swap_size" ] && echo "$NAME: swap filesize is $swap_size"
[ -n "$extra_debs_dir" ] && echo "$NAME: install extra packages from $extra_debs_dir into destination /"
[ -n "$extra_files_dir" ] && echo "$NAME: copy extra files from $extra_files_dir into destination /"
echo -e "-------------------------------------------------------------------------------"
echo -e "$NAME: device $dev is currently hold the following data:\n"
fdisk -l $dev | sed -e 's/^/   /'
echo -e "-------------------------------------------------------------------------------"
lsblk $dev
echo -e "\n$NAME: press ENTER to continue erasing ALL DATA, or just hit CTRL-C to"
echo "$NAME: stop here! :-)"
read ans

umount $devp1
umount $devp2

set -e

echo "$NAME: erasing device..."
dd if=/dev/zero of=$dev bs=512 count=1 2>/dev/null

echo "$NAME: formatting device..."
echo -e 'p\nn\np\n1\n8192\n+16M\nn\np\n2\n40960\n\nt\n1\nc\nw\n' | \
		fdisk $dev >/dev/null

if [ -n "$u_boot_img" ] ; then
	if [ ! -f $u_boot_img ] ; then
		echo "$NAME: image $u_boot_img is not a file!"
		exit 1
	fi

	echo "$NAME: writing u-boot image $u_boot_img..."
	dd if=$u_boot_img of=$dev bs=1K seek=1 2>/dev/null
fi

echo "$NAME: building boot partition..."
mkfs.vfat -n boot $devp1 2>/dev/null 1>&2

if [ -n "$kernel_img" -o -n "$dtb_img" ] ; then
	mount $devp1 /mnt/

	if [ -n "$kernel_img" ] ; then
		if [ ! -f $kernel_img ] ; then
			echo "$NAME: image $kernel_img is not a file!"
			exit 1
		fi

		echo "$NAME: writing kernel image $kernel_img..."
		cp $kernel_img /mnt/
	fi

	if [ -n "$dtb_img" ] ; then
		if [ ! -f $dtb_img ] ; then
			echo "$NAME: image $dtb_img is not a file!"
			exit 1
		fi

		echo "$NAME: writing DTB image $dtb_img..."
		cp $dtb_img /mnt/
	fi

	echo "$NAME: syncing $devp1..."
	umount /mnt
fi

echo "$NAME: checking boot partition..."
fsck.vfat -a $devp1

echo "$NAME: building root partition..."
yes | mkfs.ext4 -L root $devp2 2>/dev/null 1>&2
tune2fs -O has_journal -o journal_data_ordered $devp2 >/dev/null
tune2fs -O dir_index $devp2 >/dev/null

if [ -n "$rootfs_img" -o -n "$mod_fw_h_img" -o -n "$swap_size" -o \
     -n "$extra_debs_dir" -o -n "$extra_files_dir" ] ; then
	mount $devp2 /mnt/

	if [ -n "$rootfs_img" ] ; then
		if [ ! -f $rootfs_img ] ; then
			echo "$NAME: image $rootfs_img is not a file!"
			exit 1
		fi

		echo "$NAME: writing rootfs image $rootfs_img..."
		tar -C /mnt/ --strip-components=1 -xzf $rootfs_img
	fi

	if [ -n "$mod_fw_h_img" ] ; then
		if [ ! -f $mod_fw_h_img ] ; then
			echo "$NAME: image $mod_fw_h_img is not a file!"
			exit 1
		fi

		echo "$NAME: writing modules, firmware & headers image $mod_fw_h_img..."
		tar -C /mnt/ -xzf $mod_fw_h_img
	fi
	
	if [ -n "$swap_size" ] ; then
		if [ ! -d /mnt/etc/ ] ; then
			echo "$NAME: destination /etc directory is missing!" \
			     "Try adding --rootfs"
			exit 1
		fi

		echo "$NAME: adding the swap file..."
		fallocate -l $swap_size /mnt/swap
		chown root:root /mnt/swap
		chmod 0600 /mnt/swap
		mkswap /mnt/swap >/dev/null
		echo "/swap none swap defaults 0 0" >> /mnt/etc/fstab
	fi

	if [ -n "$extra_debs_dir" ] ; then
		if [ ! -d $extra_debs_dir ] ; then
			echo "$NAME: name $extra_debs_dir is not a directory!"
			exit 1
		fi

		echo "$NAME: installing packages from $extra_debs_dir into /..."
		cp -a $extra_debs_dir/* /mnt/tmp/
		chroot /mnt bash -c "dpkg -i /tmp/*.deb"
	fi
	

	if [ -n "$extra_files_dir" ] ; then
		if [ ! -d $extra_files_dir ] ; then
			echo "$NAME: name $extra_files_dir is not a directory!"
			exit 1
		fi

		echo "$NAME: copying content of $extra_files_dir into /..."
		cp -a $extra_files_dir/* /mnt/
	fi
	
	echo "$NAME: syncing $devp2..."
	umount /mnt
fi

echo "$NAME: checking root partition..."
fsck.ext4 -D $devp2

echo "$NAME: done!"
exit 0
